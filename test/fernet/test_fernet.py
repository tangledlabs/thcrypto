import pytest

from time import time

from thcrypto import Fernet


@pytest.fixture(scope='module')
def generated_fernet():
    generated_fernet: Fernet = Fernet.generate().unwrap()
    assert isinstance(generated_fernet, Fernet)

    return generated_fernet


def test_generate_fernet():
    """
    This function test generate fernet key - success expected
    """
    fernet_: Fernet = Fernet.generate().unwrap()
    assert isinstance(fernet_, Fernet)


def test_load_or_generate_file():
    """
    This function tests fernet key load or generate when file dont exist.
    Function generates fernet key and writes it down to the file - success expected
    """
    fernet_: Fernet = Fernet.load_or_generate_file().unwrap()
    assert isinstance(fernet_, Fernet)


def test_load_or_generate_file_load():
    """
    This function tests fernet key load or generate. Fist function call generates fernet key
    and writes it down to the file, while second function call loads fernet key from previously
    generated file - success expected
    """
    generated_fernet: Fernet = Fernet.load_or_generate_file().unwrap()
    loaded_fernet: Fernet = Fernet.load_or_generate_file().unwrap()
    assert isinstance(loaded_fernet, Fernet)


def test_encrypt_bytes(generated_fernet):
    """
    This function tests bytes encryption with fernet key - success expected
    """
    # class method test
    encrypted_bytes: bytes = Fernet.encrypt_bytes(generated_fernet, b'test bytes', ).unwrap()
    assert isinstance(encrypted_bytes, bytes)

    # instance method test
    encrypted_bytes: bytes = generated_fernet.encrypt_bytes(b'test bytes', ).unwrap()
    assert isinstance(encrypted_bytes, bytes)


def test_encrypt_bytes_current_time_passed(generated_fernet):
    """
    This function tests bytes encryption with fernet key when current_time param passed - success expected
    """
    test_bytes = b'test bytes'

    # class method test
    encrypted_bytes: bytes = Fernet.encrypt_bytes(generated_fernet,
                                                  data_bytes=test_bytes,
                                                  current_time=int(time())).unwrap()
    assert isinstance(encrypted_bytes, bytes)

    # instance method test
    encrypted_bytes: bytes = generated_fernet.encrypt_bytes(data_bytes=test_bytes, current_time=int(time())).unwrap()
    assert isinstance(encrypted_bytes, bytes)


def test_decrypt_bytes(generated_fernet):
    """
    This function tests bytes decryption with fernet key- success expected
    """
    test_bytes = b'test bytes'
    encrypted_bytes: bytes = generated_fernet.encrypt_bytes(test_bytes).unwrap()
    assert isinstance(encrypted_bytes, bytes)

    # class method test
    decrypted_bytes: bytes = Fernet.decrypt_bytes(generated_fernet, encrypted_bytes).unwrap()
    assert isinstance(decrypted_bytes, bytes)
    assert decrypted_bytes == test_bytes

    # instance method test
    decrypted_bytes: bytes = generated_fernet.decrypt_bytes(encrypted_bytes).unwrap()
    assert isinstance(decrypted_bytes, bytes)
    assert decrypted_bytes == test_bytes


def test_decrypt_bytes_ttl_and_current_time_passed(generated_fernet):
    """
    This function tests bytes decryption with fernet key when ttl and current_time params passed - success expected
    """
    test_bytes = b'test bytes'
    encrypted_bytes: bytes = generated_fernet.encrypt_bytes(data_bytes=test_bytes,
                                                            current_time=int(time())).unwrap()
    assert isinstance(encrypted_bytes, bytes)

    # class method test
    decrypted_bytes: bytes = Fernet.decrypt_bytes(generated_fernet,
                                                  enc_data_bytes=encrypted_bytes,
                                                  ttl=100,
                                                  current_time=int(time())).unwrap()
    assert decrypted_bytes == test_bytes

    # instance method test
    decrypted_bytes: bytes = generated_fernet.decrypt_bytes(enc_data_bytes=encrypted_bytes,
                                                            ttl=100,
                                                            current_time=int(time())).unwrap()
    assert isinstance(decrypted_bytes, bytes)
    assert decrypted_bytes == test_bytes


def test_decrypt_bytes_ttl_passed(generated_fernet):
    """
    This function tests bytes decryption with fernet key when ttl param passed - success expected
    """
    test_bytes = b'test bytes'
    encrypted_bytes: bytes = generated_fernet.encrypt_bytes(data_bytes=test_bytes,
                                                            current_time=int(time())).unwrap()
    assert isinstance(encrypted_bytes, bytes)

    # class method test
    decrypted_bytes: bytes = Fernet.decrypt_bytes(generated_fernet,
                                                  enc_data_bytes=encrypted_bytes,
                                                  ttl=100).unwrap()
    assert isinstance(decrypted_bytes, bytes)
    assert decrypted_bytes == test_bytes

    # instance method test
    decrypted_bytes: bytes = generated_fernet.decrypt_bytes(enc_data_bytes=encrypted_bytes,
                                                            ttl=100).unwrap()
    assert isinstance(decrypted_bytes, bytes)
    assert decrypted_bytes == test_bytes


def test_encrypt_dict(generated_fernet):
    """
    This function tests encryption with fernet key on dict - success expected
    """
    test_dict = {'test_key': 'test_value'}

    # class method test
    encrypted_dict: str = Fernet.encrypt_dict(generated_fernet, test_dict).unwrap()
    assert isinstance(encrypted_dict, str)

    # class method test
    encrypted_dict: str = generated_fernet.encrypt_dict(test_dict).unwrap()
    assert isinstance(encrypted_dict, str)


def test_encrypt_dict_current_time_passed(generated_fernet):
    """
    This function tests encryption with fernet key on dict when current_time param passed- success expected
    """
    test_dict = {'test_key': 'test_value'}

    # class method test
    encrypted_dict: str = Fernet.encrypt_dict(generated_fernet,
                                              data_dict=test_dict,
                                              current_time=int(time())).unwrap()
    assert isinstance(encrypted_dict, str)

    # instance method test
    encrypted_dict: str = generated_fernet.encrypt_dict(data_dict=test_dict,
                                                        current_time=int(time())).unwrap()
    assert isinstance(encrypted_dict, str)


def test_decrypt_dict(generated_fernet):
    """
    This function tests decryption with fernet key on dict - success expected
    """
    test_dict = {'test_key': 'test_value'}
    encrypted_dict: str = generated_fernet.encrypt_dict(test_dict).unwrap()
    assert isinstance(encrypted_dict, str)

    # class method test
    decrypted_dict: dict = Fernet.decrypt_dict(generated_fernet, encrypted_dict).unwrap()
    assert isinstance(decrypted_dict, dict)
    assert decrypted_dict == test_dict

    # class method test
    decrypted_dict: dict = generated_fernet.decrypt_dict(encrypted_dict).unwrap()
    assert isinstance(decrypted_dict, dict)
    assert decrypted_dict == test_dict


def test_decrypt_dict_ttl_and_current_time_passed(generated_fernet):
    """
    This function tests decryption with fernet key on dict when ttl and current_time params passed- success expected
    """
    test_dict = {'test_key': 'test_value'}
    encrypted_dict: str = generated_fernet.encrypt_dict(data_dict=test_dict,
                                                        current_time=int(time())).unwrap()
    assert isinstance(encrypted_dict, str)

    # class method test
    decrypted_dict: dict = Fernet.decrypt_dict(generated_fernet,
                                               enc_data_str=encrypted_dict,
                                               ttl=100,
                                               current_time=int(time())).unwrap()
    assert isinstance(decrypted_dict, dict)
    assert decrypted_dict == test_dict

    # instance method test
    decrypted_dict: dict = generated_fernet.decrypt_dict(enc_data_str=encrypted_dict,
                                                         ttl=100,
                                                         current_time=int(time())).unwrap()
    assert isinstance(decrypted_dict, dict)
    assert decrypted_dict == test_dict


def test_decrypt_dict_ttl_passed(generated_fernet):
    """
    This function tests decryption with fernet key on dict when ttl param passed- success expected
    """
    test_dict = {'test_key': 'test_value'}
    encrypted_dict: str = generated_fernet.encrypt_dict(data_dict=test_dict,
                                                        current_time=int(time())).unwrap()
    assert isinstance(encrypted_dict, str)

    # class method test
    decrypted_dict: dict = Fernet.decrypt_dict(generated_fernet,
                                               enc_data_str=encrypted_dict,
                                               ttl=100).unwrap()
    assert isinstance(decrypted_dict, dict)
    assert decrypted_dict == test_dict

    # instance method test
    decrypted_dict: dict = generated_fernet.decrypt_dict(enc_data_str=encrypted_dict,
                                                         ttl=100).unwrap()
    assert isinstance(decrypted_dict, dict)
    assert decrypted_dict == test_dict
