import pytest

from thcrypto import SECP256K1
from cryptography.exceptions import InvalidSignature


@pytest.fixture(scope='module')
def generated_secp256k1():
    secp256k1_generated: SECP256K1 = SECP256K1.generate().unwrap()
    assert isinstance(secp256k1_generated, SECP256K1)

    return secp256k1_generated


def test_genereate_success():
    '''
    This function tests generating private SECP256K1 key - success expected
    '''
    secp256k1_: SECP256K1 = SECP256K1.generate().unwrap()
    assert isinstance(secp256k1_, SECP256K1)


@pytest.mark.xfail(raises=FileNotFoundError, strict=True)
def test_load():
    '''
    This function loading secp256k1 key from file - FileNotFoundError expected
    '''
    private_path: str = 'sk_secp256k1.pem'
    public_path: str = 'pk_secp256k1.pem'
    secp256k1_: SECP256K1 = SECP256K1.load(private_path, public_path).unwrap()
    assert isinstance(secp256k1_, SECP256K1)


def test_load_or_generate_file_generate_case():
    '''
    This function tests load or generate secp256k1 key when file dont exist.
    Function generates secp256k1 key and writes it down to the file - success expected
    '''
    secp256k1_: SECP256K1 = SECP256K1.load_or_generate_file().unwrap()
    assert isinstance(secp256k1_, SECP256K1)


def test_load_or_generate_file_load_case():
    '''
    This function tests load or generate secp256k1 key. Fist function call generates secp256k1 key
    and writes it down to the file, while second function call loads secp256k1 key from previously
    generated file - success expected
    '''
    generated_secp256k1_: SECP256K1 = SECP256K1.load_or_generate_file().unwrap()
    loaded_secp256k1_: SECP256K1 = SECP256K1.load_or_generate_file().unwrap()
    assert isinstance(loaded_secp256k1_, SECP256K1)


def test_sign(generated_secp256k1):
    '''
    This function signing data with private key - success expected
    '''
    test_b = b'test_test_b'

    # class method test
    b: bytes = SECP256K1.sign(generated_secp256k1, test_b).unwrap()
    assert isinstance(b, bytes)

    # instance method test
    b: bytes = generated_secp256k1.sign(test_b).unwrap()
    assert isinstance(b, bytes)


@pytest.mark.xfail(raises=FileNotFoundError, strict=True)
def test_save():
    '''
    This function tests saving private and public key - FileNotFoundError expected
    '''
    secp256k1_: SECP256K1 =  SECP256K1.generate().unwrap()
    b = secp256k1_.save(private_path='', public_path='').unwrap()


def test_verify(generated_secp256k1):
    '''
    This function verify if data is signed with valid key - success expected
    '''
    test_b = b'test_test_b'

    # class method test
    signed_data: bytes = SECP256K1.sign(generated_secp256k1, test_b).unwrap()
    b: bool = SECP256K1.verify(generated_secp256k1, signed_data, test_b).unwrap()
    assert isinstance(b, bool)
    assert b

    # instance method test
    signed_data: bytes = generated_secp256k1.sign(test_b).unwrap()
    b: bool = generated_secp256k1.verify(signed_data, test_b).unwrap()
    assert isinstance(b, bool)
    assert b


@pytest.mark.xfail(raises=InvalidSignature, strict=True)
def test_verify_error(generated_secp256k1):
    '''
    This function verify if data is signed with valid key - InvalidSignature expected
    '''
    test_b = b'test_test_b'
    signed_data: bytes = generated_secp256k1.sign(test_b).unwrap()
    b = generated_secp256k1.verify(signed_data, b'test_error!!!!!!!!!').unwrap()
