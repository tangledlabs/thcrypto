import pytest

from thcrypto import Ed25519


@pytest.fixture(scope='module')
def generated_ed25519():
    ed25519_generated: Ed25519 = Ed25519.generate().unwrap()
    assert isinstance(ed25519_generated, Ed25519)

    return ed25519_generated


def test_genereate_success():
    """
    This function tests generating private ed25519 key - success expected
    """
    generated_ed25519: Ed25519 = Ed25519.generate().unwrap()
    assert isinstance(generated_ed25519, Ed25519)


def test_load_or_generate_file_generate_case():
    """
    This function tests load or generate Ed25519 when file dont exist.
    Function generates Ed25519 and writes it down to the file - success expected
    """
    generated_ed25519: Ed25519 = Ed25519.load_or_generate_file().unwrap()
    assert isinstance(generated_ed25519, Ed25519)


def test_load_or_generate_file_load_case():
    """
    This function tests load or generate Ed25519. Fist function call generates Ed25519
    and writes it down to the file, while second function call loads Ed25519 from previously
    generated file - success expected
    """
    generated_ed25519: Ed25519 = Ed25519.load_or_generate_file().unwrap()
    assert isinstance(generated_ed25519, Ed25519)

    loaded_ed25519: Ed25519 = Ed25519.load_or_generate_file().unwrap()
    assert isinstance(loaded_ed25519, Ed25519)


def test_load():
    """
    This function loading ed25519 key from file - success expected
    """
    private_path: str = 'sk_ed25519.pem'
    public_path: str = 'pk_ed25519.pem'
    loaded_ed25519: Ed25519 = Ed25519.load(private_path, public_path).unwrap()
    assert isinstance(loaded_ed25519, Ed25519)


def test_sign(generated_ed25519):
    """
    This function signing data with private key - success expected
    """
    test_b = b'test_test_b'

    # class method test
    signature: bytes = Ed25519.sign(generated_ed25519, test_b).unwrap()
    assert isinstance(signature, bytes)

    # instance method test
    signature: bytes = generated_ed25519.sign(test_b).unwrap()
    assert isinstance(signature, bytes)


def test_verify(generated_ed25519):
    """
    This function verify if data is signed with valid key - success expected
    """
    test_b = b'test_test_b'

    signature: bytes = generated_ed25519.sign(test_b).unwrap()
    assert isinstance(signature, bytes)

    # class method test
    verified: bool = Ed25519.verify(generated_ed25519, signature, test_b).unwrap()
    assert isinstance(verified, bool)

    # instance method test
    verified: bool = generated_ed25519.verify(signature, test_b).unwrap()
    assert isinstance(verified, bool)


@pytest.mark.xfail(raises=Exception, strict=True)
def test_verify_error(generated_ed25519):
    """
    This function verify if data is signed with valid key - success expected
    """
    test_b = b'test_test_b'

    signature: bytes = generated_ed25519.sign(test_b).unwrap()
    assert isinstance(signature, bytes)

    # class method test
    verified: bool = Ed25519.verify(generated_ed25519, b'signature', test_b).unwrap()
    assert isinstance(verified, bool)


def test_get_raw_private_key_bytes(generated_ed25519):
    """
    This function return private key bytes - success expected
    """
    # class method test
    private_key_bytes: bytes = Ed25519.get_raw_private_key_bytes(generated_ed25519).unwrap()
    assert isinstance(private_key_bytes, bytes)

    # instance method test
    private_key_bytes: bytes = generated_ed25519.get_raw_private_key_bytes().unwrap()
    assert isinstance(private_key_bytes, bytes)


def test_get_raw_private_key(generated_ed25519):
    """
    This function return decoded private key - success expected
    """
    # class method test
    private_key_raw: bytes = Ed25519.get_raw_private_key(generated_ed25519).unwrap()
    assert isinstance(private_key_raw, str)

    # instance method test
    private_key_raw: bytes = generated_ed25519.get_raw_private_key().unwrap()
    assert isinstance(private_key_raw, str)


def test_get_raw_public_key_bytes(generated_ed25519):
    """
    This function return public key bytes - success expected
    """
    # class method test
    raw_public_key_bytes: bytes = Ed25519.get_raw_public_key_bytes(generated_ed25519).unwrap()
    assert isinstance(raw_public_key_bytes, bytes)

    # instance method test
    raw_public_key_bytes: bytes = generated_ed25519.get_raw_public_key_bytes().unwrap()
    assert isinstance(raw_public_key_bytes, bytes)


def test_get_raw_public_key(generated_ed25519):
    """
    This function return decoded public key - success expected
    """
    # class method test
    raw_public_key: bytes = Ed25519.get_raw_public_key(generated_ed25519).unwrap()
    assert isinstance(raw_public_key, str)

    # instance method test
    raw_public_key: bytes = generated_ed25519.get_raw_public_key().unwrap()
    assert isinstance(raw_public_key, str)
