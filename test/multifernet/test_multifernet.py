import pytest

from time import time
from cryptography.fernet import Fernet as _Fernet
from cryptography.fernet import InvalidToken

from thcrypto import Fernet
from thcrypto import MultiFernet


@pytest.fixture(scope='module')
def fernet_key_store():
    fernet_key_store: list[Fernet] = [Fernet.generate().unwrap() for i in range(10)]

    return fernet_key_store


def test_multifernet_encrypt_decrypt_bytes(fernet_key_store):
    """
    This function tests bytes encryption and decryption with multifernet key - success expected
    """
    multi_fernet: MultiFernet = MultiFernet(fernet_key_store)

    # class method test
    for i in range(100):
        msg: bytes = b'12345'
        enc_msg: bytes = MultiFernet.encrypt_bytes(multi_fernet, msg).unwrap()
        dec_msg: bytes = MultiFernet.decrypt_bytes(multi_fernet, enc_msg).unwrap()
        assert msg == dec_msg

    # instance method test
    for i in range(100):
        msg: bytes = b'12345'
        enc_msg: bytes = multi_fernet.encrypt_bytes(msg).unwrap()
        dec_msg: bytes = multi_fernet.decrypt_bytes(enc_msg).unwrap()
        assert msg == dec_msg


def test_multifernet_encrypt_decrypt_bytes_current_time(fernet_key_store):
    """
    This function tests bytes encryption and decryption with multifernet key -
    when current_time param passed to encrypting function and current_time param
    passed to decrypting function (current_time param is being ignored
    when ttl param is not present) - success expected
    """
    multi_fernet: MultiFernet = MultiFernet(fernet_key_store)

    # class method test
    for i in range(100):
        msg: bytes = b'12345'
        enc_msg: bytes = MultiFernet.encrypt_bytes(multi_fernet, msg, current_time=int(time())).unwrap()
        dec_msg: bytes = MultiFernet.decrypt_bytes(multi_fernet, enc_msg, current_time=int(time())).unwrap()
        assert msg == dec_msg

    # class method test
    for i in range(100):
        msg: bytes = b'12345'
        enc_msg: bytes = multi_fernet.encrypt_bytes(msg, current_time=int(time())).unwrap()
        dec_msg: bytes = multi_fernet.decrypt_bytes(enc_msg, current_time=int(time())).unwrap()
        assert msg == dec_msg


def test_multifernet_encrypt_bytes_current_time_decrypt_bytes_ttl_current_time(fernet_key_store):
    """
    This function tests bytes encryption and decryption with multifernet key -
    when current_time param passed to encrypting function and ttl and current_time params
    passed to decrypting function - success expected
    """
    multi_fernet: MultiFernet = MultiFernet(fernet_key_store)

    # class method test
    for i in range(100):
        msg: bytes = b'12345'
        enc_msg: bytes = MultiFernet.encrypt_bytes(multi_fernet, msg, current_time=int(time())).unwrap()
        dec_msg: bytes = MultiFernet.decrypt_bytes(multi_fernet, enc_msg, ttl=100, current_time=int(time())).unwrap()
        assert msg == dec_msg

    # instance method test
    for i in range(100):
        msg: bytes = b'12345'
        enc_msg: bytes = multi_fernet.encrypt_bytes(msg, current_time=int(time())).unwrap()
        dec_msg: bytes = multi_fernet.decrypt_bytes(enc_msg, ttl=100, current_time=int(time())).unwrap()
        assert msg == dec_msg


def test_multifernet_encrypt_decrypt_bytes_ttl(fernet_key_store):
    """
    This function tests bytes encryption adn decryption with multifernet key -
    when current_time param passed to encrypting function and ttl param
    passed to decrypting function - success expected
    """
    multi_fernet: MultiFernet = MultiFernet(fernet_key_store)

    # class method test
    for i in range(100):
        msg: bytes = b'12345'
        enc_msg: bytes = MultiFernet.encrypt_bytes(multi_fernet, msg, current_time=int(time())).unwrap()
        dec_msg: bytes = MultiFernet.decrypt_bytes(multi_fernet, enc_msg, ttl=100).unwrap()
        assert msg == dec_msg

    # instance method test
    for i in range(100):
        msg: bytes = b'12345'
        enc_msg: bytes = multi_fernet.encrypt_bytes(msg, current_time=int(time())).unwrap()
        dec_msg: bytes = multi_fernet.decrypt_bytes(enc_msg, ttl=100).unwrap()
        assert msg == dec_msg


def test_multifernet_encrypt_decrypt_dict(fernet_key_store):
    """
    This function tests dict encryption and decryption with multifernet key - success expected
    """
    multi_fernet: MultiFernet = MultiFernet(fernet_key_store)

    # class method test
    for i in range(100):
        test_dict: dict = {'test_key': 'test_value'}
        enc_dict: str = MultiFernet.encrypt_dict(multi_fernet, test_dict).unwrap()
        dec_dict: dict = MultiFernet.decrypt_dict(multi_fernet, enc_dict).unwrap()
        assert test_dict == dec_dict

    # instance method test
    for i in range(100):
        test_dict: dict = {'test_key': 'test_value'}
        enc_dict: str = multi_fernet.encrypt_dict(test_dict).unwrap()
        dec_dict: dict = multi_fernet.decrypt_dict(enc_dict).unwrap()
        assert test_dict == dec_dict


def test_multifernet_encrypt_decrypt_dict_current_time(fernet_key_store):
    """
    This function tests dict encryption and decryption with multifernet key -
    when current_time param passed to encrypting function and current_time param
    passed to decrypting function (current_time param is being ignored
    when ttl param is not present) - success expected
    """
    multi_fernet: MultiFernet = MultiFernet(fernet_key_store)

    # class method test
    for i in range(100):
        test_dict: dict = {'test_key': 'test_value'}
        enc_dict: str = MultiFernet.encrypt_dict(multi_fernet, test_dict, current_time=int(time())).unwrap()
        dec_dict: dict = MultiFernet.decrypt_dict(multi_fernet, enc_dict, current_time=int(time())).unwrap()
        assert test_dict == dec_dict

    # instance method test
    for i in range(100):
        test_dict: dict = {'test_key': 'test_value'}
        enc_dict: str = multi_fernet.encrypt_dict(test_dict, current_time=int(time())).unwrap()
        dec_dict: dict = multi_fernet.decrypt_dict(enc_dict, current_time=int(time())).unwrap()
        assert test_dict == dec_dict


def test_multifernet_encrypt_dict_current_time_decrypt_dict_ttl_current_time(fernet_key_store):
    """
    This function tests dict encryption adn decryption with multifernet key -
    when current_time param passed to encrypting function and ttl and current_time params
    passed to decrypting function - success expected
    """
    multi_fernet: MultiFernet = MultiFernet(fernet_key_store)

    # class method test
    for i in range(100):
        test_dict: dict = {'test_key': 'test_value'}
        enc_dict: str = MultiFernet.encrypt_dict(multi_fernet, test_dict, current_time=int(time())).unwrap()
        dec_dict: dict = MultiFernet.decrypt_dict(multi_fernet, enc_dict, ttl=100, current_time=int(time())).unwrap()
        assert test_dict == dec_dict

    # instance method test
    for i in range(100):
        test_dict: dict = {'test_key': 'test_value'}
        enc_dict: str = multi_fernet.encrypt_dict(test_dict, current_time=int(time())).unwrap()
        dec_dict: dict = multi_fernet.decrypt_dict(enc_dict, ttl=100, current_time=int(time())).unwrap()
        assert test_dict == dec_dict


def test_multifernet_encrypt_decrypt_dict_ttl(fernet_key_store):
    """
    This function tests dict encryption adn decryption with multifernet key -
    when current_time param passed to encrypting function and ttl param
    passed to decrypting function - success expected
    """
    multi_fernet: MultiFernet = MultiFernet(fernet_key_store)

    # class method test
    for i in range(100):
        test_dict: dict = {'test_key': 'test_value'}
        enc_dict: str = MultiFernet.encrypt_dict(multi_fernet, test_dict, current_time=int(time())).unwrap()
        dec_dict: dict = MultiFernet.decrypt_dict(multi_fernet, enc_dict, ttl=100).unwrap()
        assert test_dict == dec_dict

    # instance method test
    for i in range(100):
        test_dict: dict = {'test_key': 'test_value'}
        enc_dict: str = multi_fernet.encrypt_dict(test_dict, current_time=int(time())).unwrap()
        dec_dict: dict = multi_fernet.decrypt_dict(enc_dict, ttl=100).unwrap()
        assert test_dict == dec_dict


def test_multifernet_add_fernet(fernet_key_store):
    """
    This function tests add_fernet - success expected
    """
    # class method test
    mf1: MultiFernet = MultiFernet(fernet_key_store)
    assert all(isinstance(n, _Fernet) for n in mf1._multi_fernet._fernets)

    new_fernet: Fernet = Fernet.generate().unwrap()
    mf2: MultiFernet = MultiFernet.add_fernet(mf1, new_fernet).unwrap()
    assert all(isinstance(n, _Fernet) for n in mf2._multi_fernet._fernets)
    assert mf1 is not mf2
    assert len(mf1._multi_fernet._fernets) != len(mf2._multi_fernet._fernets)

    # instance method test
    mf1: MultiFernet = MultiFernet(fernet_key_store)
    assert all(isinstance(n, _Fernet) for n in mf1._multi_fernet._fernets)

    new_fernet: Fernet = Fernet.generate().unwrap()
    mf2: MultiFernet = mf1.add_fernet(new_fernet).unwrap()
    assert all(isinstance(n, _Fernet) for n in mf2._multi_fernet._fernets)
    assert mf1 is not mf2
    assert len(mf1._multi_fernet._fernets) != len(mf2._multi_fernet._fernets)


def test_multifernet_encrypt_decrypt_bytes_rotate_keys(fernet_key_store):
    """
    This function tests dict encryption adn decryption with rotated multifernet key - success expected
    """
    mf_1: MultiFernet = MultiFernet(fernet_key_store)
    assert mf_1
    assert isinstance(mf_1, MultiFernet)
    assert len(mf_1._multi_fernet._fernets) == len(fernet_key_store)

    msg: bytes = b'12345'

    # Encrypt and decrypt msg with mf_1
    enc_msg_with_mf_1: bytes = mf_1.encrypt_bytes(msg).unwrap()
    dec_msg_with_mf_1: bytes = mf_1.decrypt_bytes(enc_msg_with_mf_1).unwrap()
    assert msg == dec_msg_with_mf_1

    # Create new fernet and create new MultiFernet with all existing kyes and add new key to beginning of list
    new_fernet: Fernet = Fernet.generate().unwrap()
    mf_2: MultiFernet = mf_1.add_fernet(new_fernet).unwrap()
    assert mf_2 and isinstance(mf_2, MultiFernet)
    assert len(mf_2._multi_fernet._fernets) != len(mf_1._multi_fernet._fernets)

    rotated_msg_with_mf_2: bytes = mf_2.rotate(enc_msg_with_mf_1).unwrap()

    # mf_2 is able to decrypt rotated token and also token encrypted with mf_1
    for i in range(100):
        dec_msg: bytes = mf_2.decrypt_bytes(rotated_msg_with_mf_2).unwrap()
        assert msg == dec_msg

    for i in range(100):
        dec_msg: bytes = mf_2.decrypt_bytes(enc_msg_with_mf_1).unwrap()
        assert msg == dec_msg


@pytest.mark.xfail(raises=InvalidToken, strict=True)
def test_multifernet_decrypt_with_old_multi_fernet_fails_after_key_rotation(fernet_key_store):
    """
    This function tests bytes encryption and decryption with rotated multifernet key -
    InvalidToken exception expected when decryption attempt with old multifernet - success expected
    """
    mf_1: MultiFernet = MultiFernet(fernet_key_store)
    assert mf_1
    assert isinstance(mf_1, MultiFernet)
    assert len(mf_1._multi_fernet._fernets) == len(fernet_key_store)

    msg: bytes = b'12345'

    # Encrypt and decrypt msg with mf_1
    enc_msg_with_mf_1: bytes = mf_1.encrypt_bytes(msg).unwrap()
    dec_msg_with_mf_1: bytes = mf_1.decrypt_bytes(enc_msg_with_mf_1).unwrap()
    assert msg == dec_msg_with_mf_1

    # Create new fernet, add to fernet_key_store and create new multi_fernet
    new_fernet: Fernet = Fernet.generate().unwrap()
    mf_2: MultiFernet = mf_1.add_fernet(new_fernet).unwrap()
    assert mf_2 and isinstance(mf_2, MultiFernet)
    assert len(mf_2._multi_fernet._fernets) != len(mf_1._multi_fernet._fernets)

    rotated_msg_with_mf_2: bytes = mf_2.rotate(enc_msg_with_mf_1).unwrap()

    # multi_fernet_1 is NOT ABLE to decrypt rotated token, raises InvalidToken exception
    dec_msg_mf1: bytes = mf_1.decrypt_bytes(rotated_msg_with_mf_2).unwrap()
