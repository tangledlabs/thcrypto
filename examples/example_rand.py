from thcrypto import (
    gen_random_int,
    gen_random_int_hex,
    gen_random_int_hex_bytes,
    gen_random_int128,
    gen_random_int128_hex,
    gen_random_int128_hex_bytes,
    gen_random_int256,
    gen_random_int256_hex,
    gen_random_int256_hex_bytes
)


# Generate a random integer, based on passed number of bits
random_int_bits: int = gen_random_int(256).unwrap()


# Generate random string, based on passed number of bits
random_int_hex: str = gen_random_int_hex(256).unwrap()


# Generate random bytes, based on passed number of bits
random_int_hex_bytes: bytes = gen_random_int_hex_bytes(128).unwrap()


# Generate a random integer, based on 128 bits
random_int_128: int = gen_random_int128().unwrap()


# Generate random string, based on 128 bits
random_int_128_hex: str = gen_random_int128_hex().unwrap()


# Generate random bytes, based on 128 bits
random_int_128_hex_bytes: bytes = gen_random_int128_hex_bytes().unwrap()


# Generate a random integer, based on 256 bits
random_int_256: int = gen_random_int256().unwrap()


# Generate random string, based on 256 bits
random_int_256_hex: str =  gen_random_int256_hex().unwrap()


# Generate random bytes, based on 256 bits
random_int_256_hex_bytes: bytes = gen_random_int256_hex_bytes().unwrap()




