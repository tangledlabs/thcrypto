from thcrypto import SECP256K1


# Generate SECP256K1
generated_secp256k1: SECP256K1 = SECP256K1.generate().unwrap()


# Load secp256k1 keys from files - if files not exists then generates SECP256K1,
# writes private and public keys to the files and returns SECP256K1
# Parameters (private_path: str='custom_private_path.pem', public_path: str='custom_public_path.pem') are optional,
# if not passed then default params are used
# (private_path: str='sk_secp256k1.pem', public_path: str='pk_secp256k1.pem')
secp256k1_: SECP256K1 = SECP256K1.load_or_generate_file().unwrap()


# Sign data with secp256k1 private key
signed_data: bytes = secp256k1_.sign(b'test_test_b').unwrap()


# Verify signature with secp256k1 public key
verified: bool = secp256k1_.verify(signed_data, b'test_test_b').unwrap()
