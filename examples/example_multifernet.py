from time import time

from thcrypto import Fernet
from thcrypto import MultiFernet


# Initialize MultiFernet
multi_fernet_1: MultiFernet = MultiFernet([Fernet.generate().unwrap() for i in range(10)])


# Encrypt bytes
# This function takes required argument 'data_bytes' and optional argument 'current_time'
# data_bytes: bytes, current_time: int | None=None)
encrypted_bytes: bytes = multi_fernet_1.encrypt_bytes(data_bytes=b'12345',
                                                      current_time=int(time())).unwrap()

# Decrypt bytes
# This function takes required argument 'enc_data_bytes' and two optional arguments 'ttl' and 'current_time'
# (enc_data_bytes: bytes, ttl: int | None=None, current_time: int | None=None)
decrypted_bytes: bytes = multi_fernet_1.decrypt_bytes(enc_data_bytes=encrypted_bytes,
                                                      ttl=100,
                                                      current_time=int(time())).unwrap()

# Encrypt dict
# This function takes required argument 'data_dict' and optional argument 'current_time'
# data_dict: bytes, current_time: int | None=None)
encrypted_dict: str = multi_fernet_1.encrypt_dict(data_dict={'test_key': 'test_value'},
                                                  current_time=int(time())).unwrap()

# Decrypt dict
# This function takes required argument 'enc_data_bytes' and two optional arguments 'ttl' and 'current_time'
# (enc_data_str: bytes, ttl: int | None=None, current_time: int | None=None)
decrypted_dict: dict = multi_fernet_1.decrypt_dict(enc_data_str=encrypted_dict,
                                                   ttl=100,
                                                   current_time=int(time())).unwrap()


# Add new fernet
# This function takes required argument 'fernet' and returns new instance of MultiFernet with new fernet key
# added to beginning of fernet key list
new_fernet: Fernet = Fernet.generate().unwrap()
multi_fernet_2: MultiFernet = multi_fernet_1.add_fernet(new_fernet).unwrap()


# Rotate fernet keys (re-encrypt token)
# This function takes required argument 'encrypted_bytes'
rotated_msg_with_mf_2: bytes = multi_fernet_2.rotate(encrypted_bytes).unwrap()
