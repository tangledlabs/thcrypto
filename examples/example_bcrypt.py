from thcrypto import Bcrypt


# Generate bcrypt salt with default parameters (rounds: int=12, prefix: bytes=b'2b')
bcrypt_default: Bcrypt = Bcrypt.generate().unwrap()


# Generate bcrypt salt with custom parameters (4 < rounds: int > 31, prefix: bytes elem {b'2a', b'2b'}
# If custom parameters are not passed, default values are used
bcrypt_custom: Bcrypt = Bcrypt.generate(rounds=12, prefix=b'2b').unwrap()


# Load bcrypt from file; if file does not exist, generate bcrypt key and writes it down to the file
# This function can be called without parameters (in which case it is going to use
# default params (path: str='bcrypt.salt', rounds: int=12, prefix: bytes=b'2b')) or with custom params
# (path: str='custom_path', 4 < rounds: int > 31, prefix: bytes elem {b'2a', b'2b'})
# In case that bcrypt is loaded from existing file, passed custom 'rounds' and 'prefix' params are ignored
bcrypt_: Bcrypt = Bcrypt.load_or_generate_file().unwrap()


# Hash password function accepts parameter(unhashed_password: bytes | str) which is max 72 bytes long
hashed_password: bytes = Bcrypt.get_hashed_password(bcrypt_, 'test bcrypt').unwrap()
# or
hashed_password_: bytes = bcrypt_.get_hashed_password('test bcrypt').unwrap()


# Check password function accepts parameters(unhashed_password: bytes | str, hashed_password: bytes | str)
result: bool = Bcrypt.check_password('test bcrypt', hashed_password).unwrap()




